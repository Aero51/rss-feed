package rssFeed;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.value.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker.State;
import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.web.*;
import javafx.stage.Stage;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Display Contents From Database
 *
 * @author cdea
 */
public class Prikaz extends Application {
     VBox leftArea = new VBox(10);
   
    private int id = 0;
   private int pozicija=0;
     List<RssFeed> listaRssFeedova = ManipulacijaBaze.loadFeeds();

    @Override
    public void start(final Stage stage) {
        Group root = new Group();
        final Scene scene = new Scene(root, 640, 480, Color.WHITE);
        final Map<String, Hyperlink> hyperLinksMap = new TreeMap<>();


        final WebView newsBrief = new WebView(); // upper right
          final WebEngine webEngine = new WebEngine();
        final WebView websiteView = new WebView(); // lower right

        webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
            public void changed(ObservableValue<? extends State> observable, State oldValue, State newValue) {
                if (newValue != State.SUCCEEDED) {
                    return;
                }

                RssFeed rssFeed = parse(webEngine.getDocument(), webEngine.getLocation());
                System.out.println("webEngine.getDocument():" + webEngine.getDocument());
                System.out.println("webengine get location:" + webEngine.getLocation());
                System.out.println("rssFeed.channelTitle:" + rssFeed.channelTitle);
                System.out.println(" rssFeed.news.size():" + rssFeed.news.size());

                hyperLinksMap.get(webEngine.getLocation()).setText(rssFeed.channelTitle);

                // print feed info:

                StringBuilder rssSource = new StringBuilder();

                rssSource.append("<head>\n")
                        .append("</head>\n")
                        .append("<body>\n");
                rssSource.append("<b>")
                        .append(rssFeed.channelTitle)
                        .append(" (")
                        .append(rssFeed.news.size())
                        .append(")")
                        .append("</b><br />\n");
                StringBuilder htmlArticleSb = new StringBuilder();
                for (NewsArticle article : rssFeed.news) {
                    //enchanced for petlja
                    htmlArticleSb.append("<hr />\n")
                            .append("<b>\n")
                            .append(article.title)
                            .append("</b><br />")
                            .append(article.pubDate)
                            .append("<br />")
                            .append(article.description)
                            .append("<br />\n")
                            .append("<input type=\"button\" onclick=\"alert('")
                            .append(article.link)
                            .append("')\" value=\"Pregledaj\" />\n");
                }

                String content = rssSource.toString() + "<form>\n" + htmlArticleSb.toString() + "</form></body>\n";
                System.out.println(content);

                newsBrief.getEngine().loadContent(content);
                // write to disk if not already.
                ManipulacijaBaze.saveRssFeed(rssFeed);
                // MySQLAccess.saveRssFeed(rssFeed);
            }
        }); // end of webEngine addListener()

        newsBrief.getEngine().setOnAlert(new EventHandler<WebEvent<String>>() {
            public void handle(WebEvent<String> evt) {
                websiteView.getEngine().load(evt.getData());
            }
        }); // end of newsBrief setOnAlert()

        // Left and right split pane
        SplitPane splitPane = new SplitPane();
        splitPane.prefWidthProperty().bind(scene.widthProperty());
        splitPane.prefHeightProperty().bind(scene.heightProperty());



        final ContextMenu cm = new ContextMenu();
        MenuItem cmStavka1 = new MenuItem("Izbriši");
        cmStavka1.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                System.out.println("izbrisano olfa");
                ManipulacijaBaze.izbrisiRed(id);
              leftArea.getChildren().remove(pozicija+1);
              
                  System.out.println("leftArea.getChildren().size "+  leftArea.getChildren().size());
                  System.out.println("pozicija:______________"+pozicija);

                cm.hide();
            }
        });
        cm.getItems().add(cmStavka1);





      //  
        final TextField txtUpis = new TextField();
        txtUpis.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent ae) {
                final String url = txtUpis.getText();
                final Hyperlink jfxHyperLink = createHyperLink(url, webEngine);
                hyperLinksMap.put(url, jfxHyperLink);

                System.out.println("urlField.getText():" + txtUpis.getText());

                HBox rowBox = new HBox(20);
              //  ChoiceBox choice = new ChoiceBox();
                
                
    jfxHyperLink.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getButton() == MouseButton.SECONDARY) {
                        System.out.println("Radi______________________________________________________");
                        cm.show(leftArea, event.getScreenX(), event.getScreenY());
                          //pozicija
                         
                        //  jfxHyperLink.getText()
                         // if(url.)
                        
                        listaRssFeedova.clear();
                        listaRssFeedova = ManipulacijaBaze.loadFeeds();
                        
                        for(int i=0;i<listaRssFeedova.size();i++)
                        {
                              System.out.println("url.equals(listaRssFeedova.get(i).link"+ url.equals(listaRssFeedova.get(i).link));
                        if(url.equals(listaRssFeedova.get(i).link))
                        {
                            System.out.println("pozicija izbrisana: "+i);
                          id=  listaRssFeedova.get(i).link.hashCode();
                         pozicija=i;   
                       
                        }
                        }
                       
                  
                          
                    }
                  

                }
            });
                rowBox.getChildren().add(jfxHyperLink);
               // rowBox.getChildren().add(choice);


                leftArea.getChildren().add(rowBox);
                webEngine.load(url);
                txtUpis.setText("");
            }
        }); // end of txtUpis setOnAction()

        leftArea.getChildren().add(txtUpis);

       
      //  for (final RssFeed feed : listaRssFeedova) {
        for(int i=0;i<listaRssFeedova.size();i++) {
            final RssFeed feed =listaRssFeedova.get(i);
            HBox rowBox = new HBox(20);
            rowBox.setVisible(true);
            System.out.println("Feed:" + feed);


            final Hyperlink jfxHyperLink = new Hyperlink(feed.channelTitle);
            jfxHyperLink.setUserData(feed);
            final String location = feed.link;
            hyperLinksMap.put(feed.link, jfxHyperLink);
            jfxHyperLink.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent evt) {
                    webEngine.load(location);
                    id = feed.id;

                }
            });


                 final int brojac=i;

            jfxHyperLink.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getButton() == MouseButton.SECONDARY) {
                        System.out.println("Radi______________________________________________________");
                        cm.show(leftArea, event.getScreenX(), event.getScreenY());
                          //pozicija
                          System.out.println("rssFeed"+feed);
                        //  jfxHyperLink.getText()
                          
                         pozicija=brojac;
                      
                          
                    }
                  

                }
            });

            rowBox.getChildren().add(jfxHyperLink);


            leftArea.getChildren().add(rowBox);

        }
        leftArea.setAlignment(Pos.TOP_LEFT);

        // Upper and lower split pane
        SplitPane splitPane2 = new SplitPane();
        splitPane2.setOrientation(Orientation.VERTICAL);
        splitPane2.prefWidthProperty().bind(scene.widthProperty());
        splitPane2.prefHeightProperty().bind(scene.heightProperty());

        HBox centerArea = new HBox();

        centerArea.getChildren().add(newsBrief);

        HBox rightArea = new HBox();

        rightArea.getChildren().add(websiteView);

        splitPane2.getItems().add(centerArea);
        splitPane2.getItems().add(rightArea);

        // add left area
        splitPane.getItems().add(leftArea);

        // add right area
        splitPane.getItems().add(splitPane2);
        newsBrief.prefWidthProperty().bind(scene.widthProperty());
        websiteView.prefWidthProperty().bind(scene.widthProperty());
        // evenly position divider
     /*   ObservableList<SplitPane.Divider> dividers = splitPane.getDividers();
         for (int i = 0; i < dividers.size(); i++) {
         dividers.get(i).setPosition((i + 1.0) / 3);
         }*/

        HBox hbox = new HBox();
        hbox.getChildren().add(splitPane);
        root.getChildren().add(hbox);

        stage.setScene(scene);
        stage.show();


    } // end of start()

    private static RssFeed parse(Document doc, String location) {

        RssFeed rssFeed = new RssFeed();
        rssFeed.link = location;

        rssFeed.channelTitle = doc.getElementsByTagName("title")
                .item(0)
                .getTextContent();

        NodeList items = doc.getElementsByTagName("item");
        for (int i = 0; i < items.getLength(); i++) {
            Map<String, String> childElements = new HashMap<>();
            NewsArticle article = new NewsArticle();
            for (int j = 0; j < items.item(i).getChildNodes().getLength(); j++) {
                Node node = items.item(i).getChildNodes().item(j);
                childElements.put(node.getNodeName().toLowerCase(), node.getTextContent());
            }
            article.title = childElements.get("title");
            article.description = childElements.get("description");
            article.link = childElements.get("link");
            article.pubDate = childElements.get("pubdate");

            rssFeed.news.add(article);
        }

        return rssFeed;
    } // end of parse()

    private Hyperlink createHyperLink(String url, final WebEngine webEngine) {
        final Hyperlink jfxHyperLink = new Hyperlink("Loading News...");
        RssFeed aFeed = new RssFeed();
        aFeed.link = url;

        jfxHyperLink.setUserData(aFeed);
        jfxHyperLink.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent evt) {
                RssFeed rssFeed = (RssFeed) jfxHyperLink.getUserData();
                webEngine.load(rssFeed.link);

            }
        });
        return jfxHyperLink;
    }

    public static void main(String[] args) throws Exception {

        //R  MySQLAccess dao= new MySQLAccess();
        // dao.readDataBase();
        System.out.append("dsfdsfdsf");
        ManipulacijaBaze.setupDb();


        // MySQLAccess.setupDb();
        Application.launch(args);
    }
}

class RssFeed {

    int id;
    String channelTitle = "News...";
    String link;
    List<NewsArticle> news = new ArrayList<>();

    public String toString() {
        return "RssFeed{" + "id=" + id + ", channelTitle=" + channelTitle + ", link=" + link + ", news=" + news + '}';
    }

    public RssFeed() {
    }

    public RssFeed(String title, String link) {
        this.channelTitle = title;
        this.link = link;
    }
}

class NewsArticle {

    String title;
    String description;
    String link;
    String pubDate;

    public String toString() {
        return "NewsArticle{" + "title=" + title + ", description=" + description + ", link=" + link + ", pubDate=" + pubDate + ", enclosure=" + '}';
    }
}