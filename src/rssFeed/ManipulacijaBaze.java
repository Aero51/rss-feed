package rssFeed;

import java.sql.*;
import java.util.*;


public class ManipulacijaBaze {
    // connection properties

    final static Properties props = new Properties();

    static {
        props.put("user", "nsrdoc");
        props.put("password", "nsrdoc123");

    }
    // private static String framework = "embedded";
    private static String driver = "com.mysql.jdbc.Driver";
    private static String protocol = "jdbc:mysql:";

    public static void setupDb() {
        loadDriver();
        Connection conn = null;
        ArrayList statements = new ArrayList();
        Statement s = null;
        ResultSet rs = null;
        try {
            // database name
            String dbName = "nsrdoc";

            conn = DriverManager.getConnection("jdbc:mysql://ucka.veleri.hr:3306", "nsrdoc", "nsrdoc123");

            System.out.println("Creating database " + dbName);
            boolean createTable = false;
            s = conn.createStatement();
            try {
                s.executeQuery("SELECT count(*) FROM nsrdoc.rssFeed");
            } catch (Exception e) {
                System.out.println("nema table, izbrisana je");
                createTable = true;
            }

            if (createTable == false) {

                // handle transaction
                conn.setAutoCommit(false);

                s = conn.createStatement();
                statements.add(s);

                // za kreiranje tablice
                s.execute("create table rssFeed(id int, title varchar(255), url varchar(600))");
                System.out.println("Created table rssFeed ");

                conn.commit();
            }

            shutdown();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {

            close(rs);

            // Statements and PreparedStatements
            int i = 0;
            while (!statements.isEmpty()) {
                // PreparedStatement extend Statement
                Statement st = (Statement) statements.remove(i);
                close(st);
            }

            close(conn);

        }

    }

    public static List<RssFeed> loadFeeds() {
        loadDriver();

        Connection conn = null;
        ResultSet rs = null;
        List<RssFeed> feeds = new ArrayList<>();
        try {
            // database name
            String dbName = "nsrdoc";

            conn = DriverManager.getConnection("jdbc:mysql://ucka.veleri.hr:3306", "nsrdoc", "nsrdoc123");

            rs = conn.createStatement().executeQuery("select id, title, url from nsrdoc.rssFeed");
            while (rs.next()) {
                String title = rs.getString("title");
                String url = rs.getString("url");
                RssFeed rssFeed = new RssFeed(title, url);
                rssFeed.id = rssFeed.link.hashCode();
                System.out.println("Ovo se ubacuje u id:" + rssFeed.link.hashCode());

                feeds.add(rssFeed);
            }
            shutdown();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {

            close(rs);
            close(conn);

        }
        return feeds;
    }

    private static void shutdown() {
       
    }

    public static int saveRssFeed(RssFeed rssFeed) {
        int pk = rssFeed.link.hashCode();

        loadDriver();

        Connection conn = null;
        ArrayList statements = new ArrayList();
        PreparedStatement psInsert = null;
        Statement s = null;
        ResultSet rs = null;
        try {


            // database name
            String dbName = "nsrdoc";

            conn = DriverManager.getConnection("jdbc:mysql://ucka.veleri.hr:3306", "nsrdoc", "nsrdoc123");
            //   conn = DriverManager.getConnection(protocol + dbName
            //   , props);

            rs = conn.createStatement().executeQuery("select count(id) from nsrdoc.rssFeed where id = " + rssFeed.link.hashCode());

            rs.next();
            int count = rs.getInt(1);

            if (count == 0) {

                // handle transaction
                conn.setAutoCommit(false);

                s = conn.createStatement();
                statements.add(s);

                psInsert = conn.prepareStatement("insert into nsrdoc.rssFeed values (?, ?, ?)");
                statements.add(psInsert);
                psInsert.setInt(1, pk);
                String escapeTitle = rssFeed.channelTitle.replaceAll("\'", "''");
                psInsert.setString(2, escapeTitle);
                psInsert.setString(3, rssFeed.link);
                psInsert.executeUpdate();
                conn.commit();
                System.out.println("Inserted " + rssFeed.channelTitle + " " + rssFeed.link);
                System.out.println("Committed the transaction");
            }
            shutdown();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
         

            // ResultSet
            close(rs);

            // Statements and PreparedStatements
            int i = 0;
            while (!statements.isEmpty()) {
                // PreparedStatement extend Statement
                Statement st = (Statement) statements.remove(i);
                close(st);
            }

            //Connection
            close(conn);

        }



        return pk;
    }

    private static void close(AutoCloseable closable) {
        try {
            if (closable != null) {
                closable.close();
                closable = null;
            }
        } catch (Exception sqle) {
            sqle.printStackTrace();
        }
    }

    private static void loadDriver() {

        try {

            Class.forName(driver);
            System.out.println("Loaded driver");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void izbrisiRed(int id) {
        System.out.println("An example for Deleting a Row from a Database!");
        Connection con = null;



        try {
            Class.forName(driver);
            con = DriverManager.getConnection("jdbc:mysql://ucka.veleri.hr:3306", "nsrdoc", "nsrdoc123");
            try {



                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;
                //  resultSet = preparedStatement.executeQuery();

                preparedStatement = con.prepareStatement("delete from nsrdoc.rssFeed where id= ? ; ");
                preparedStatement.setString(1, String.valueOf(id));
                preparedStatement.executeUpdate();



            } catch (SQLException s) {
                System.out.println("SQL upit nije izvrsen!");
                s.printStackTrace();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


    }
}
